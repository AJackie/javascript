var map = L.map('map').setView([19.432608, -99.133209], 2);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var cities = [
		{
			'latitud': 37.387453,
			'longitud': -122.057716,
			'lugar': 'Silicon Valley'
		},
		{
			'latitud': 20.659401,
			'longitud': -103.350352,
			'lugar': 'Guadalajara'
		},
		{
			'latitud': 25.687223,
			'longitud': -100.316291,
			'lugar': 'Monterrey'
		},
		{
			'latitud': 19.431313,
			'longitud': -99.136853,
			'lugar': 'CDMX'
		},
		{
			'latitud': 22.378272,
			'longitud': 114.127776,
			'lugar': 'Honk Kong'
		},
		{
			'latitud': 19.855248,
			'longitud': -90.528473,
			'lugar': 'McCarthys Campeche'
		}
];
//cada iteración esta tomando un array con latitud, longitus y lugar
cities.forEach(function(elemento){
	L.marker([elemento['latitud'], elemento['longitud']]).addTo(map)
	.bindPopup(elemento['lugar'])
	.openPopup();
});
/*

//funciona
for(i = 0; i < cities.length; i++){
	L.marker([cities[i]['latitud'], cities[i]['longitud']]).addTo(map)
	.bindPopup(cities[i]['lugar'])
	.openPopup();
	console.log(' ' + cities[i]['latitud'] + ', ' + cities[i]['longitud'] + ', ' + cities[i]['lugar']);
}
for(i = 0; i < cities.length; i++){
	console.log(' ' + cities[i]['latitud'] + ', ' + cities[i]['longitud'] + ', ' + cities[i]['lugar']);
}

var cities = [
		{
			latittud: 37.387453,
			longitud: -122.057716,
			lugar: 'Silicon Valley'
		},
		{
			latitud: 20.659401,
			longitud: -103.350352,
			lugar: 'Guadalajara'
		},
		{
			latitud: 25.687223,
			longitud: -100.316291,
			lugar: 'Monterrey'
		},
		{
			latitud: 19.431313,
			longitud: -99.136853,
			lugar: 'CDMX'
		},
		{
			latitud: 22.378272,
			longitud: 114.127776,
			lugar: 'Honk Kong'
		},
		{
			latitud: 19.855248,
			longitud: -90.528473,
			lugar: 'McCarthys Campeche'
		}
	];

cities2 = [
	'silicon valley',
	'guadalajara',
	'monterrey',
	'CDMX',
	'honk kong',
	'MCCarthis Campeche']


L.marker([19.8454, -90.5237]).addTo(map)
.bindPopup('Campechido.')
.openPopup();
*/  