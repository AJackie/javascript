$(function() {
	console.log("ready")

	$('#sum').click(function(e){
		$('#operaciones').parsley()

		var first_number = $('#first_number').val()
			second_number = $('#second_number').val()
			result = $('#result')
		
		e.preventDefault()
		first = parseInt(first_number)
		second = parseInt(second_number)
		if (first_number == '') {
			window.alert('Por favor, ingrese el primer valor')
		} else if (second_number == ''){
			window.alert('Por favor, ingrese el segundo valor')
		}else if (first > -1 && second > -1 ) {
			sum_result = first + second
			result.val(sum_result)
		} else {
			window.alert('Uno de los números es negativos, por favor use solo use números positivos')
		}
			
	})
	$('#resta').click(function(e){
		$('#operaciones').parsley()

		var first_number = $('#first_number').val()
			second_number = $('#second_number').val()
			result = $('#result')

		e.preventDefault()
		first = parseInt(first_number)
		second = parseInt(second_number)
		if (first_number == '') {
			window.alert('Por favor, ingrese el primer valor')
		} else if (second_number == ''){
			window.alert('Por favor, ingrese el segundo valor')
		}else if (first > -1 && second > -1 ) {
			res_result = first - second
			result.val(res_result)
		} else {
			window.alert('Uno de los números es negativos, por favor use solo use números positivos')
		}
			
	})

	$('#mul').click(function(e){
		$('#operaciones').parsley()

		var first_number = $('#first_number').val()
			second_number = $('#second_number').val()
			result = $('#result')

		e.preventDefault()
		first = parseInt(first_number)
		second = parseInt(second_number)
		if (first_number == '') {
			window.alert('Por favor, ingrese el primer valor')
		} else if (second_number == ''){
			window.alert('Por favor, ingrese el segundo valor')
		}else {
			mul_result = first * second
			result.val(mul_result)	
		}	
	})

	$('#div').click(function(e){
		$('#operaciones').parsley()

		var first_number = $('#first_number').val()
			second_number = $('#second_number').val()
			result = $('#result')

		e.preventDefault()
		first = parseInt(first_number)
		second = parseInt(second_number)
		if (first_number == '') {
			window.alert('Por favor, ingrese el primer valor')
		} else if (second_number == ''){
			window.alert('Por favor, ingrese el segundo valor')
		}else {
			div_result = first / second
			result.val(div _result)	
		}
	})
		

});
